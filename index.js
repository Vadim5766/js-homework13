let timer = setInterval(slide, 3000);

let buttonStop = document.createElement("button");
buttonStop.textContent = "Припинити";
buttonStop.classList.add("buttonStop");
buttonStop.style.cssText =
  "padding:10px;margin-top:20px;margin-right:10px;background-color:orange;border-radius:10px;font-size:20px";
buttonStop.type = "button";
let buttonStart = document.createElement("button");
buttonStart.textContent = "Відновити показ";
buttonStart.classList.add("buttonStart");
buttonStart.style.cssText =
  "padding:10px;background-color:orange;border-radius:10px;font-size:20px";
buttonStart.type = "button";
document.body.append(buttonStop, buttonStart);

let image = document.querySelectorAll(".image-to-show");
let i = 0;

function slide() {
  image[i].style.display = "none";
  if (i === image.length - 1) {
    i = 0;
    image[0].style.display = "block";
  } else {
    image[i + 1].style.display = "block";
    i++;
  }
}

let playBtn = document.querySelector(".buttonStart");
playBtn.disabled = true;

playBtn.addEventListener("click", () => {
  playBtn.disabled = true;
  timer = setInterval(slide, 3000);
});
document.querySelector(".buttonStop").addEventListener("click", () => {
  playBtn.disabled = false;
  clearInterval(timer);
});
